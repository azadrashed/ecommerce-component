<?php
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\DashboardController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix("admin")->group(function()

{
    Route::get('/',[DashboardController::class,'home']);
    Route::get('/table',[DashboardController::class,'table']);
    Route::get('/products',[ProductController::class,'index'])->name('products.index');
    Route::get('/products/create',[ProductController::class,'create'])->name('products.create');

});
Route::controller(PublicController::class)->group(function(){
    Route::get('/home','home')->name('homepage');   
    Route::get('/product','product')->name('productpage');   
    Route::get('/computer','computer')->name('computerpage');   
    Route::get('/checkout','checkout')->name('checkout');   
    Route::get('/categories','categories')->name('categories');   
    Route::get('/order','order')->name('order');   
    Route::get('/cart','cart')->name('cart');   
}
);
// Route::prefix("")->group(function()

// {
//     Route::get('/',[PublicController::class,'home']);
//     Route::get('/table',[PublicController::class,'table']);
//     // Route::get('/products',[ProductController::class,'index'])->name('products.index');
//     // Route::get('/products/create',[ProductController::class,'create'])->name('products.create');

// });
